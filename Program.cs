﻿using System;
using System.Threading.Tasks;

namespace FridayClass 
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await MyOtherMethodAsync();
            await MyMethodAsync();
           
        }
        public static async Task MyMethodAsync()
        {
            Task<int> longRunningTask = HeavyComputationAsync();
            int result = await longRunningTask;
            Console.WriteLine(result);
        }
        public static async Task MyOtherMethodAsync()
        {
            Task<int> shortRunningTask = LightComputationAsync();
            int answer = await shortRunningTask;
            Console.WriteLine(answer);
        }
           

        
        public static async Task<int> HeavyComputationAsync()
        {
            await Task.Delay(20000);//(1000);
                return 1;
        }
        public static async Task<int> LightComputationAsync()
        {
            await Task.Delay(1000);//(20000);
            return 5;
        }
    }
}
